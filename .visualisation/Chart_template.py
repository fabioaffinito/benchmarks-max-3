import jinja2
import os
import csv
import pandas as pd
import json
import sys
import argparse
import copy
import shutil
import glob

def render_index(tpl_path, context):
    path, filename = os.path.split(tpl_path)

    environment = jinja2.Environment(undefined=jinja2.StrictUndefined,
    loader=jinja2.FileSystemLoader(path or '.')) #It means jinja will take templates of environment from file in the introduced path
    # Include url_for_.... in the context
    context['url_for_index'] = 'index.html'
    context['url_for_qe'] = 'qe.html'
    context['url_for_yambo'] = 'yambo.html'
    
    return environment.get_template(filename).render(context)

def render_qe(tpl_path, context):
    path, filename = os.path.split(tpl_path)

    environment = jinja2.Environment(undefined=jinja2.StrictUndefined,
    loader=jinja2.FileSystemLoader(path or '.')) #
    # Include url_for_... in the context
    context['url_for_index'] = 'index.html'
    context['url_for_qe'] = 'qe.html'
    context['url_for_yambo'] = 'yambo.html'

    return environment.get_template(filename).render(context)

def render_yambo(tpl_path, context):
    path, filename = os.path.split(tpl_path)

    environment = jinja2.Environment(undefined=jinja2.StrictUndefined,
    loader=jinja2.FileSystemLoader(path or '.')) #
    # Include url_for_index in the context
    context['url_for_index'] = 'index.html'
    context['url_for_qe'] = 'qe.html'
    context['url_for_yambo'] = 'yambo.html'
    
    return environment.get_template(filename).render(context)

def render_template(template_path, entry, entry_info, table_columns, platform, path_of_input, context):
    # Load the template environment
    template_loader = jinja2.FileSystemLoader(searchpath=os.path.dirname(template_path))
    template_env = jinja2.Environment(loader=template_loader, autoescape=True)


    def load_csv_data(entry):
        return entry.to_dict(orient='list')

    template_env.filters['load_csv_data'] = load_csv_data

    def to_list(series, format_numbers=False):
        if format_numbers:
            return [float(f'{num:.2f}') for num in series.tolist()]
        else:
            return series.tolist()
    
    template_env.filters['to_list'] = to_list
    context['table_columns'] = table_columns
    context['Ltime_unit'] = entry_info['time_unit']   
    context['Lx_axis'] = entry_info['x_axis']
    context['Lcolumn_name'] = entry_info['column_name']
    context['Lcomponent'] = entry_info['component']
    context['system'] = entry_info['system']
    context['platform'] = platform
    context['path_of_input'] = path_of_input 
    # Load the template
    template = template_env.get_template(os.path.basename(template_path))

    # Include the dataframe and column_names in the context
    context['dataframe'] = entry
    context['x'] = entry[entry_info['x_axis']]
    context['column_name'] = entry[entry_info['column_name']]
    context['output_path'] = entry_info['output_path']
    if entry_info['component'] != 'empty':
        context['component'] = entry[entry_info['component']]
        context['other_comp'] = entry[entry_info['column_name']] - entry[entry_info['component']]
        
    for column in table_columns:
        context[f'{column}'] = entry[column]
    # Render the template with the provided context
    output_html = template.render(context)

    # Write the rendered HTML to the output file
    with open(entry_info['output_path'], 'w') as output_file:
        output_file.write(output_html)


def gen_index():
    entries = {}
    with open ('index.html', 'w') as f:
        f.write(render_index('.visualisation/templates/index.tmpl', {'entries': entries}))

def gen_qe(loaded_dataframes):
    # Create the context to be used in the template
    context = {
       # 'page_title': page_title,
        #'path_of_input': destination_path,
    }
    entries = []
    # Accessing the dataframes and associated information
    for df_name, df_info in loaded_dataframes.items():
        dataframe = df_info['dataframe']
        output_path = df_info['output_path']
        system_structure_name = df_info['system']
        path_of_input = df_info['path_of_input']
        code = df_info['code']
        machine = df_info['platform']
        version = df_info['version']
        date = df_info['date']
        info1 = df_info['info']
        if code=='qe':

            table_columns = []
            for i in range(len(dataframe.columns)):
                table_columns.append(dataframe.columns[i])
            # Render the template and save the output HTML
            render_template(".visualisation/templates/chart_modify.tmpl", dataframe, df_info, table_columns, machine, path_of_input, context)

            _, basename = os.path.split(output_path)
            name, _ = os.path.splitext(basename)
            entry = {
                'workload': system_structure_name,
                'info': info1,
                'platform': machine,
                'date': date,
                'version': version,
                'file': output_path,
                'name': name,
                # Add more parameters as needed
                }
            entries.append(entry)
    with open ('qe.html', 'w') as f:
        f.write(render_qe('.visualisation/templates/qe.tmpl', {'entries': entries}))
        
def gen_yambo(loaded_dataframes):
    # Create the context to be used in the template
    context = {
    #    'page_title': page_title,
        'path_of_input': destination_path,
    }
    entries = []
    # Accessing the dataframes and associated information
    for df_name, df_info in loaded_dataframes.items():
        dataframe = df_info['dataframe']
        output_path = df_info['output_path']
        system_structure_name = df_info['system']
        path_of_input = df_info['path_of_input']
        code = df_info['code']
        machine = df_info['platform']
        version = df_info['version']
        date = df_info['date']
        info1 = df_info['info']
        if code=='yambo':

            table_columns = []
            for i in range(len(dataframe.columns)):
                table_columns.append(dataframe.columns[i])

            # Render the template and save the output HTML    
            render_template(".visualisation/templates/chart_modify.tmpl", dataframe, df_info, table_columns, machine,path_of_input, context)

            _, basename = os.path.split(output_path)
            name, _ = os.path.splitext(basename)
            entry = {
                'workload': system_structure_name,
                'info': info1,
                'platform': machine,
                'date': date,
                'version': version,
                'file': output_path,
                'name': name,
                # Add more parameters as needed
                }
            entries.append(entry)
    with open ('yambo.html', 'w') as f:
        f.write(render_yambo('.visualisation/templates/yambo.tmpl', {'entries': entries}))
    
def count_files(directory, file_extensions=None):
    """Count the number of files and get the files in the provided directory with specific extensions"""
    try:
        # List all items in the directory
        items = os.listdir(directory)

        # Filter out only files with the specified extensions
        if file_extensions:
            items = [item for item in items if os.path.isfile(os.path.join(directory, item)) and any(item.endswith(ext) for ext in file_extensions)]

        # Count the number of files
        file_count = len(items)

        return file_count, items
    except OSError:
        # Handle the case where the directory doesn't exist or is not accessible
        return -1, []

def code_recogniser(file_path, keywords):
    # Extract the folder names from the file path
    folders = file_path.split(os.path.sep)

    # Find the folder with any of the specified keywords
    found_keywords = [folder for folder in folders if any(keyword.lower() in folder.lower() for keyword in keywords)]

    if found_keywords:
        #print(f"Found keywords {found_keywords} in the folder path.")
        return found_keywords[0]
    else:
        print(f"No specified keywords found in the file path.")
        return None

def platform_recogniser(file_path, machines):
    # Extract the folder names from the file path
    folders = file_path.split(os.path.sep)

    # Find the folder with any of the specified keywords
    found_machines = [folder for folder in folders if any(keyword.lower() in folder.lower() for keyword in machines)]

    if found_machines:
        #print(f"Found keywords {found_machines} in the folder path.")
        return found_machines[0]
    else:
        print(f"No specified keywords found in the file path.")
        return None


def recogniser(file_path, position):
    # Extract the folder names from the file path
    folders = file_path.split(os.path.sep)

    # Check if the specified position is valid
    if 0 <= position < len(folders):
        return folders[position]
    else:
        print(f"Invalid position {position}. Please provide a valid position.")
        return None


def dataframes_maker(dataframes_list, **kwargs):
    loaded_dataframes = {}
    for num, df_dict in enumerate(dataframes_list, start=1):
        df_name = f'df_{num}'
        dataframe = pd.read_csv(df_dict['filename'], sep=';')
        code = df_dict['code']
        system=df_dict['system']
        time_unit= df_dict['time_unit']
        if time_unit == 'second':
            dataframe.iloc[:, 3:] /= 1
        elif time_unit == 'minute':
            dataframe.iloc[:, 3:] /= 60
        elif time_unit == 'hour':
            dataframe.iloc[:, 3:] /= 3600
        else:
            print(f"Error: Invalid time unit '{time_unit}' specified.")
        
        # Add new information to the dataframe dictionary
        _, basename = os.path.split(df_dict['filename'])
        name, _ = os.path.splitext(basename)
        # Split the name based on '-'
        name_parts = name.split('-')

        # Check if there are at least two parts (assuming you want to split into at least two parts)
        if len(name_parts) >= 2:
            kind = name_parts[1]
            dataframe_info = {
            'dataframe': dataframe,
            'x_axis': df_dict['x_axis'],
            'column_name': df_dict['column_name'],
            'time_unit': df_dict['time_unit'],
            'component': df_dict['component'],
            'code': df_dict['code'],
            'system': df_dict['system'],
            'output_path': kwargs.get('output_path', f'./{system}_{kind}.html'),
            'path_of_input': df_dict['path_of_input'],
            'platform': df_dict['platform'],
            'version': df_dict['version'],
            'date': df_dict['date'],
            'info': f'{kind}'
        }
        else:
            kind = "_" 
            dataframe_info = {
            'dataframe': dataframe,
            'x_axis': df_dict['x_axis'],
            'column_name': df_dict['column_name'],
            'time_unit': df_dict['time_unit'],
            'component': df_dict['component'],
            'code': df_dict['code'],
            'system': df_dict['system'],
            'output_path': kwargs.get('output_path', f'./{system}.html'),
            'path_of_input': df_dict['path_of_input'],
            'platform': df_dict['platform'],
            'version': df_dict['version'],
            'date': df_dict['date'],
            'info': f'{kind}'
        }

        
        loaded_dataframes[df_name] = copy.deepcopy(dataframe_info)

    return loaded_dataframes


if __name__ == "__main__":
    # Define the data for filling in the template
    page_title = "Benchmarking results"
    print('Welcome Message:')
    print('\t Flask code for visualizing benchmark results of the MAX project.')
    print('\tArguments: file_names or directores, x_axis, column_name, time_unit, component(any_column or empty)')
    print('\tAuthor: Mandana Safari \n')
    if len(sys.argv) < 2:
        print('Inserting all the parameters is mandetory (x_axis,column_name,time_unit,component)')
        print('USAGE with directories: python Chart_template.py --directories ./yambo=Nodes,walltime,second,electrons ./qe=Nodes,walltime,second,sth_kernel ...')
        print('USAGE with files: python Chart_templategen.py --files ./yambo/result0.dat=Nodes,walltime,second,sth_kernel ./yambo/result1.dat=Nodes,walltime,second,electrons ...')
        print('More information with: python Chart_template.py --help')
    
    # Modify the argument parsing logic
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--files', nargs='+', type=str, help='List of file specifications with associated x_axis, column_name, time_unit, and component (use equal sign to separate file path and arguments)')
    parser.add_argument('--directories', '-dirs', nargs='+', type=str, help='List of directories specifications with associated x_axis, column_name, time_unit, and component (use equal sign to separate directory and arguments)')
    parser.add_argument('--position', type=int, default=2, help='Position parameter for recogniser function')

    args = parser.parse_args()

    position = args.position
    
    dataframes_list = []

    keywords_to_search = ['yambo', 'qe', 'othercode']
    machines = ['leonardo-boost', 'lumi', 'marconi']

    if args.directories:
        for dir_spec in args.directories:
            dir_info = {}
            dir_parts = dir_spec.split('=')
            dir_info['directory'] = dir_parts[0]

            if len(dir_parts) > 1:
                properties = dir_parts[1].split(',')
                dir_info['x_axis'] = properties[0] or 'Nodes'
                dir_info['column_name'] = properties[1] or 'walltime'
                dir_info['time_unit'] = properties[2] or 'second'
                dir_info['component'] = properties[3] or 'empty'

            # Extract code from the folder structure
            code = None

            code = code_recogniser(dir_info['directory'], keywords_to_search)
            machine = platform_recogniser(dir_info['directory'], machines)

            dir_info['code'] = code
                
            #dataframes_list.append(dir_info)


            file_count, files = count_files(dir_info['directory'],file_extensions=['.txt', '.dat'])
            if file_count == -1:
                print(f"Error: Unable to access or find the directory {dir_info['directory']}.")
            else:
                print(f"The number of files in the directory {dir_info['directory']} is: {file_count}")
                for file_path in files:
                    print(f"Processing file: {file_path}")
                    x_axis = dir_info['x_axis'] or 'Nodes'
                    column_name = dir_info['column_name'] or 'walltime'
                    time_unit = dir_info['time_unit'] or 'second'
                    component = dir_info['component'] or 'empty'

                    path_input_pattern = os.path.join(dir_info['directory'], 'inputfiles', '*.in')
                    # Find all .in files in the 'inputfiles' directory
                    in_files = glob.glob(path_input_pattern)

                    if not in_files:
                        print(f"No .in files found in the 'inputfiles' directory for {dir_info['directory']}.")

                    else:
                        # Assuming you want to choose the first .in file found
                        source_path = in_files[0]

                    system_structure_name = recogniser(dir_info['directory'], position)
                    version_date = recogniser(dir_info['directory'], position+2)
                    version= version_date.split('_')[0]
                    date = version_date.split('_')[1]
                    if not system_structure_name:
                        #print(f"System structure name: {system_structure_name}")
                        system_structure_name = 'empty'

                    # Copy the scf.in file to the current directory
                    destination_path = os.path.join('./', f'{system_structure_name}_scf.in')
                    shutil.copy(source_path, destination_path)
                    print(destination_path)

                    dataframes_list.append({'filename': os.path.join(dir_info['directory'], file_path), 
                                            'x_axis': x_axis, 'column_name': column_name, 'time_unit': time_unit, 
                                            'component': component, 'code': code, 'system': system_structure_name,
                                            'path_of_input': destination_path, 'platform': machine, 'version': version, 'date': date})

    elif args.files:
        for file_spec in args.files:
            file_info = {}
            file_parts = file_spec.split('=')
            file_info['filename'] = file_parts[0]

            if len(file_parts) > 1:
                properties = file_parts[1].split(',')
                file_info['x_axis'] = properties[0] or 'Nodes'
                file_info['column_name'] = properties[1] or 'walltime'
                file_info['time_unit'] = properties[2] or 'second'
                file_info['component'] = properties[3] or 'empty'

            # Extract code from the folder structure
            code = None

            code = code_recogniser(file_info['filename'], keywords_to_search)
            machine = platform_recogniser(file_info['filename'], machines)
            version_date = recogniser(file_info['filename'], position+2)
            print(version_date)
            version= version_date.split('_')[0]
            date = version_date.split('_')[1]
            system_structure_name = recogniser(file_info['filename'], position)
            path_, filename = os.path.split(file_info['filename'])
            path_input_pattern = os.path.join(path_, 'inputfiles', '*.in')
            # Find all .in files in the 'inputfiles' directory
            in_files = glob.glob(path_input_pattern)

            if not in_files:
                print(f"No .in files found in the 'inputfiles' directory for {filename}.")

            else:
                # Assuming you want to choose the first .in file found
                source_path = in_files[0]

            # Copy the scf.in file to the same directory as the Python script
            destination_path = os.path.join('./', f'{system_structure_name}_scf.in')
            shutil.copy(source_path, destination_path)
            file_info['path_of_input']= destination_path
            file_info['code'] = code
            file_info['version'] = version
            file_info['date'] = date
            file_info['platform'] = machine
            file_info['system'] = system_structure_name
            if not system_structure_name:
                # print(f"System structure name: {system_structure_name}")
                system_structure_name = 'empty'

            dataframes_list.append(file_info)
    else:
        print("Please provide either --directories or --files as arguments.")

    #print(dataframes_list)

    loaded_dataframes = dataframes_maker(dataframes_list)
    print(loaded_dataframes)

    gen_index()
    gen_qe(loaded_dataframes)
    gen_yambo(loaded_dataframes)


    
    
